package pl.sda.designpatterns.bridge.zad2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class VehicleController {
    private static List<IVecicle> vecicles = new ArrayList<IVecicle>();

    public static void main(String[] args) {
        vecicles = Arrays.asList(new Car("Polonez"), new Boat("Suzuki"), new Aircraft("Fly"));
        Scanner sc = new Scanner(System.in);
         while (true){
             System.out.println("enter 1,2,3,4");
             String s = sc.nextLine();
             if (s.equals("1")){
//                 for (IVecicle v:vecicles
//                      ) {
//                     v.moveLeft();
//                 }
                 vecicles.forEach(v->v.moveLeft());
             } else if (s.equals("2")){
//                 for (IVecicle v:vecicles
//                      ) {
//                     v.moveRight();
//                 }
                 vecicles.forEach(v->v.moveRight());
             } else if (s.equals("3")){
//                 for (IVecicle v:vecicles
//                      ) {
//                     v.moveUp();
//                 }
                 vecicles.forEach(v->v.moveUp());
             } else if (s.equals("4")){
//                 for (IVecicle v: vecicles
//                      ) {
//                     v.moveDown();
//                 }
                 vecicles.forEach(v->v.moveDown());
             } else break;
         }
    }


}
