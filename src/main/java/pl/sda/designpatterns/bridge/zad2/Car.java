package pl.sda.designpatterns.bridge.zad2;

public class Car implements IVecicle {
    private String name;

    public Car(String name) {
        this.name = name;
    }

    @Override
    public void moveLeft() {
        System.out.println(name+" car left");
    }

    @Override
    public void moveRight() {
        System.out.println(name+" car right");
    }

    @Override
    public void moveUp() {
        System.out.println(name+" car up");
    }

    @Override
    public void moveDown() {
        System.out.println(name+" car down");
    }
}
