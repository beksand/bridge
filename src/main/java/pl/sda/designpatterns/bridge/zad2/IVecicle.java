package pl.sda.designpatterns.bridge.zad2;

public interface IVecicle {
    void moveLeft();
    void moveRight();
    void moveUp();
    void moveDown();
}
