package pl.sda.designpatterns.bridge.zad2;

public class Boat implements IVecicle {
    private String name;

    public Boat(String name) {
        this.name = name;
    }

    @Override
    public void moveLeft() {
        System.out.println(name+" boat left");
    }

    @Override
    public void moveRight() {
        System.out.println(name+" boat right");
    }

    @Override
    public void moveUp() {
        System.out.println(name+" boat up");
    }

    @Override
    public void moveDown() {
        System.out.println(name+" boat down");
    }
}
