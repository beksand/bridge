package pl.sda.designpatterns.bridge.zad2;

public class Aircraft implements IVecicle {
    private String name;

    public Aircraft(String nmae) {
        this.name = nmae;
    }

    @Override
    public void moveLeft() {
        System.out.println(name+" aircraft left");
    }

    @Override
    public void moveRight() {
        System.out.println(name+" aircraft right");
    }

    @Override
    public void moveUp() {
        System.out.println(name+" aircraft up");
    }

    @Override
    public void moveDown() {
        System.out.println(name+" aircraft down");
    }
}
