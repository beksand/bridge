package pl.sda.designpatterns.bridge.zad1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MyFilePrinter implements IPrinter {

    private File file;


    @Override
    public void printMessage(String s) throws FileNotFoundException {
        file = new File(s);
        Scanner sc = new Scanner(file);
        while (sc.hasNext()){
            String scLine = sc.next();
            System.out.println(scLine);

        }
        sc.close();
    }
}
