package pl.sda.designpatterns.bridge.zad1;

public class MyPrinter implements IPrinter{

    @Override
    public void printMessage(String s) {
        System.out.println(s);
    }
}
