package pl.sda.designpatterns.bridge.zad1;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        MySuperService mySuperService = new MySuperService(new MyPrinter());
        mySuperService.printData("Andrzej");
        MySuperService mySuperService1 = new MySuperService(new MyFilePrinter());
        mySuperService1.printData("file.txt");
    }
}
