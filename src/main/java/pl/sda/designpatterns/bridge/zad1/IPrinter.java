package pl.sda.designpatterns.bridge.zad1;

import java.io.FileNotFoundException;

public interface IPrinter {
    public void printMessage(String s) throws FileNotFoundException;
}
