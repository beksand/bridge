package pl.sda.designpatterns.bridge.zad1;

import java.io.FileNotFoundException;

public class MySuperService {
    private IPrinter printer;


    public MySuperService(IPrinter printer) {
        this.printer = printer;
    }



    public void printData(String s) throws FileNotFoundException {
        printer.printMessage(s);
    }
}
